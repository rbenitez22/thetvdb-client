/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb;

import java.time.LocalDate;
import java.util.Objects;

/**
 *A helper class to build an episode query string.
 * @author roberto
 */
public class EpisodeQuery extends Query
{
    private final int seriesId;

    private EpisodeQuery(int seriesId)
    {
        this.seriesId = seriesId;
    }

    public int getSeriesId()
    {
        return seriesId;
    }
    
    public EpisodeQuery withAiredSeason(int season)
    {
        addCriterion("airedSeason", Integer.toString(season));
        return this;
    }
    
    public EpisodeQuery withAiredEpisode(int number)
    {
        addCriterion("airedEpisode", Integer.toString(number));
        return this;
    }
    
    public EpisodeQuery withImdbId(String string)
    {
        addCriterion("imdbId", string);
        return this;
    }
    
    public EpisodeQuery withDvdSeason(int season)
    {
        addCriterion("dvdSeason", Integer.toString(season));
        return this;
    }
    
    public EpisodeQuery withDvdEpisode(int episode)
    {
        addCriterion("dvdEpisode",Integer.toString(episode));
        return this;
    }
    
    public EpisodeQuery withEpisodeNumberInSeries(int number)
    {
        addCriterion("absoluteNumber", Integer.toString(number));
        return this;
    }
    
    public EpisodeQuery withPage(int page)
    {
        addCriterion("page", Integer.toString(page));
        return this;
    }
    
    public EpisodeQuery withFirstAired(LocalDate date)
    {
        Objects.requireNonNull(date, "Date must not be null");
        addCriterion("firstAired", date.toString());
        return this;
    }
    
    /**
     * Create a query for this series.
     * 
     * <p>
     * By default, the page to retrieve is set to <code>1</code>.  Use {@link #withPage(int) } to select a different page.
     * </p>
     * @param id series id
     * @return query instance
     */
   public static EpisodeQuery forSeriesId(int id)
   {
       return new EpisodeQuery(id).withPage(1);
   }
}
