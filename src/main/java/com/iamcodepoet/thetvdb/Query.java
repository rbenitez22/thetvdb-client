/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *An abstract helper class to build queries for API data.
 * 
 * @author roberto
 */
public abstract class Query
{
    protected final Map<String, String> map = new HashMap<>();
  
    protected void addCriterion(String name, String value)
    {
        String valid = getValidCriterionValue(value);
        map.put(name, valid);
    }

    protected String getValidCriterionValue(String value) throws IllegalArgumentException
    {
        String valid;
        if (value == null || value.trim().isEmpty())
        {
            throw new IllegalArgumentException("Invalid null query critrion");
        }
        try
        {
            valid = URLEncoder.encode(value, "utf-8");
        } catch (UnsupportedEncodingException ex)
        {
            valid = value;
        }
        return valid;
    }

    public int size()
    {
        return map.size();
    }

    public boolean isEmpty()
    {
        return map.isEmpty();
    }

    public void clear()
    {
        map.clear();
    }

    public String createQueryString()
    {
        if (isEmpty())
        {
            throw new IllegalStateException("No query critera has been added");
        }
        
        return "?" + toString();
    }

    @Override
    public String toString()
    {
        if (map.isEmpty())
        {
            return "";
        }
        
        return map.entrySet().stream()
                .map(e -> String.format("%s=%s", e.getKey(), e.getValue()))
                .collect(Collectors.joining("&"));
    }
    
    public static Query emtpy()
    {
        return new Query(){};
    }
}
