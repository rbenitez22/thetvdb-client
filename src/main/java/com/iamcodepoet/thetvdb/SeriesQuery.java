/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb;

/**
 *A helper class to create a query for a series.
 * 
 * @author roberto
 */
public class SeriesQuery extends Query
{
    private static final String FIELD_NAME="name";
    private static final String FIELD_IMDB_ID="imdbId";
    private static final String FIELD_ZAP2IT_ID="zap2itId";
    private static final String FIELD_SLUG="slug";
    private static final String FIELD_LANGUAGE="Accept-Language";
    

    public SeriesQuery withName(String name)
    {
        addCriterion(FIELD_NAME, name);
        return this;
    }

    public SeriesQuery withImdbId(String id)
    {
        addCriterion(FIELD_IMDB_ID, id);
        return this;
    }
    
    public SeriesQuery withZap2ItId(String id)
    {
        addCriterion(FIELD_ZAP2IT_ID, id);
        return this;
    }
    
    public SeriesQuery withSlug(String slug)
    {
        addCriterion(FIELD_SLUG, slug);
        return this;
    }
    
    public SeriesQuery withLanguage(String language)
    {
        addCriterion(FIELD_LANGUAGE, language);
        return this;
    }
}
