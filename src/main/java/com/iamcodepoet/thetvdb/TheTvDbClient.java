/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb;

import com.iamcodepoet.thetvdb.model.Credentials;
import com.iamcodepoet.thetvdb.model.EpisodeResults;
import com.iamcodepoet.thetvdb.model.Graphic;
import com.iamcodepoet.thetvdb.model.SearchResult;
import com.iamcodepoet.thetvdb.model.Series;
import com.iamcodepoet.thetvdb.util.Mappers;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;

/**
 *A client to download TV Series information from <a href="https://api.thetvdb.com">The TV DB API</a>.
 * @author roberto
 */
public class TheTvDbClient
{
    public static final String API_DOMAIN="https://api.thetvdb.com";
    public static final String GRAPHICS_URL="https://thetvdb.com/banners";
    
    private String apiToken;
    
    private void setApiToken(String token)
    {
        this.apiToken = token;
    }
    
    public void downloadGraphic(Graphic graphic,Path local) throws IOException
    {
        String spec = GRAPHICS_URL + "/" + graphic.getFileName();
        URL url = new URL(spec);
       
        try(InputStream is = url.openStream();
                OutputStream os = Files.newOutputStream(local))
        {
            while(true)
            {
                byte[] buff = new byte[1024];
                int count =is.read(buff);
                if(count < 1)
                {
                    break;
                }
                
                os.write(buff, 0, count);
                
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Error downloading graphic. " + e.getMessage(), e);
        }
    }
    
    public Series getSeries(int seriesId)
    {
        String params = String.format("/%s", seriesId);
        HttpURLConnection conn = doGet(EndPoint.SERIES, Optional.of(params),Optional.empty());
        try
        {
            JsonObject root = readJson(conn);
            return Mappers.jsonObjectToSeries().apply(root.getJsonObject("data"));
        }
        catch(IOException e)
        {
            throw new RuntimeException("Error getting series. " + e.getMessage(), e);
        }
    }
    
    /**
     * Get episodes that match the given query and return ALL pages.
     * @param query query
     * @return results
     */
    public EpisodeResults getAllEpisodePages(EpisodeQuery query)
    {
        EpisodeResults all = getEpisodes(query);
        while(all.hasMorePages())
        {
            int nextPage = all.getNextPage();
            query.withPage(nextPage);
            EpisodeResults page = getEpisodes(query);
            all.setPage(page.getPage());
            page.getEpisodes().forEach(all::addEpisode);
        }
        
        return all;
        
    }
    
    /**
     * Get episodes that match the query and return the page indicated by {@link EpisodeQuery#withPage(int) }.
     * @param query query
     * @return  results
     */    
    public EpisodeResults getEpisodes(EpisodeQuery query)
    {
        Objects.requireNonNull(query, "Query must not be null");
        
        if(query.getSeriesId() < 0)
        {
            throw new IllegalArgumentException("Invalid Series ID");
        }
        
        Optional<Query> optQuery = query.isEmpty()? Optional.empty() : Optional.of(query);
        String queryToken = query.isEmpty()? "" : "/query";
        String params = String.format("/%s/episodes%s",query.getSeriesId(),queryToken);
        HttpURLConnection conn = getConnection(EndPoint.SERIES,Optional.of(params), optQuery, "GET");
        try
        {
            JsonObject root = readJson(conn);
            JsonObject links = root.getJsonObject("links");
            int last = links.getInt("last",0);
            int prev = links.getInt("prev",0);
            int current = prev + 1;
            
            EpisodeResults results = new EpisodeResults();
            results.setPage(current);
            results.setPageCount(last);
            
            root.getJsonArray("data").stream()
                    .map(Mappers.jsonValueToEpisode())
                    .forEach(results::addEpisode);
            
            return results;
        }
        catch(IOException e)
        {
            throw new RuntimeException("Error getting episodes. " + e.getMessage(), e);
        }
    }
    
    /**
     * Search series matching query criteria.
     * @param query query
     * @return  results
     */
    public List<SearchResult> search(SeriesQuery query)
    {
        Objects.requireNonNull(query, "Query must not be null");
        if(query.isEmpty())
        {
            throw new IllegalArgumentException("Query is empty");
        }
        
        try
        {
            HttpURLConnection conn = doGet(EndPoint.SEARCH_SERIES,Optional.of(query));
            conn.setDoInput(true);
            
            JsonObject root = readJson(conn);
            
            if(root.containsKey("data"))
            {
                return root.getJsonArray("data").stream()
                        .map(Mappers.jsonToSearchResult())
                        .collect(Collectors.toList());
            }
            
            return Collections.emptyList();
            
        } 
        catch (IOException e)
        {
            throw new RuntimeException("Error while performing search. " + e.getMessage(),e);
        }
        
    }
    
    
    /**
     * Refresh API token.
     * @return token
     */
    public String refreshToken()
    {
        try
        {
            HttpURLConnection conn = doGet(EndPoint.REFRESH_TOKEN,Optional.empty());
            readApiToken(conn);
            
            return apiToken;
            
        }
        catch(IOException e)
        {
            throw new RuntimeException("Error refreshing token. " + e.getMessage(), e);
        }
    }
    
    /**
     * Log in to <a href="https://api.thetvdb.com">The TV DB API</a> with the given credentials.
     * @param credentials credentials provided by The TV DB API
     * @return token
     */
    public String login(Credentials credentials)
    {
        Objects.requireNonNull(credentials, "Credentials must not be null");
        apiToken = null;
        URL url = EndPoint.LOGIN.getUrl();
        try
        {
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
        
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type","application/json");
            
            writeConnectionData(conn, credentials.toJson());
            
            conn.connect();
            
            readApiToken(conn);
            
            return apiToken;
            
        }
        catch(IOException e)
        {
            throw new RuntimeException("Login error. " + e.getMessage(),e);
        }
        
    }

    private void readApiToken(HttpURLConnection conn) throws RuntimeException, IOException
    {
        JsonObject json = readJson(conn);
        if(json.containsKey("Error"))
        {
            String error = json.getString("Error");
            throw new RuntimeException(error);
        }
        
        if(!json.containsKey("token"))
        {
            throw new RuntimeException("Token not found in response");
        }
        
        apiToken = json.getString("token");
    }

    private void writeConnectionData(HttpURLConnection conn, JsonObject json) throws IOException
    {
        try(OutputStream os = conn.getOutputStream();
                JsonWriter writer = Json.createWriter(os))
        {
            writer.writeObject(json);
        }
    }

    private JsonObject readJson(HttpURLConnection conn) throws IOException
    {
        JsonObject json;
        try(InputStream is = conn.getInputStream();
                JsonReader reader = Json.createReader(is))
        {
            json = reader.readObject();
        }
        return json;
    }
    
    private HttpURLConnection doGet(EndPoint endPoint,Optional<Query> query)
    {
        return doGet(endPoint, Optional.empty(), query);
    }
    
    private HttpURLConnection doGet(EndPoint endPoint,Optional<String> pathParams,Optional<Query> query)
    {
        return getConnection(endPoint,pathParams,query, "GET");
    }

    private HttpURLConnection getConnection(EndPoint endPoint,Optional<String> pathParams,Optional<Query> query, String requestMethod) 
    {
        Objects.requireNonNull(apiToken, "Cannot get a connection with a null API token");
        String queryString = query.isPresent()? query.get().createQueryString() : null;
        Optional<String> opt = Optional.ofNullable(queryString);
        
        URL url = endPoint.getUrl(pathParams,opt);
        return openConnection(url, requestMethod);
    }

    private HttpURLConnection openConnection(URL url, String requestMethod) throws RuntimeException
    {
        try
        {
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            
            String auth = String.format("Bearer %s", apiToken);
            conn.setRequestProperty("Authorization", auth);
            conn.setRequestMethod(requestMethod);
            conn.setRequestProperty("Content-Type","application/json");
            
            return conn;
            
        }
        catch(IOException e)
        {
            throw new RuntimeException("Error opening connection. " + e.getMessage(), e);
        }
    }
    
    private static enum EndPoint
    {
        LOGIN("/login"),
        REFRESH_TOKEN("/refresh_token"),
        SEARCH_SERIES("/search/series"),
        SERIES("/series");
        
       final String endpoint;

        EndPoint(String string)
        {
            this.endpoint = string;
        }
        
        public URL getUrl()
        {
            return getUrl(Optional.empty(),Optional.empty());
        }
        
        public URL getUrl(Optional<String> pathParams,Optional<String> query)
        {
            try
            {
                String spec = API_DOMAIN + endpoint + pathParams.orElse("") + query.orElse("");
                return new URL(spec);
            }
            catch(MalformedURLException e)
            {
                String msg = String.format("URL is not formed properly. ", e.getMessage());
                throw new RuntimeException(msg, e);
            }
        }
    }
    
    /**
     * Create an instance with an existing token.
     * 
     * <p>
     * It is incumbent upon to user to make sure the token has not expired.
     * </p>
     * @param apiToken token
     * @return  instance
     */
    public static TheTvDbClient withToken(String apiToken)
    {
        TheTvDbClient client = new TheTvDbClient();
        client.setApiToken(apiToken);
        
        return client;
    }
    
    /**
     * Create an instance with the given credentials.
     * @param credentials credentials provided by The TVDB API
     * @return instance
     */
    public static TheTvDbClient withCredentials(Credentials credentials)
    {
        TheTvDbClient client = new TheTvDbClient();
        client.login(credentials);
        
        return client;
    }
}
