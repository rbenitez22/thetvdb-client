/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb.model;

import javax.json.Json;
import javax.json.JsonObject;

/**
 *A data object for the credentials necessary to request a token from 
 * <a href="https://api.thetvdb.com">The TV DB API</a>.
 * @author roberto
 */
public class Credentials
{
    private String apiKey;
    private String userName;
    private String userKey;

    public String getApiKey()
    {
        return apiKey;
    }

    public void setApiKey(String apiKey)
    {
        this.apiKey = apiKey;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserKey()
    {
        return userKey;
    }

    public void setUserKey(String userKey)
    {
        this.userKey = userKey;
    }

    @Override
    public String toString()
    {
        return "Credentials{" + "apiKey=" + apiKey + ", userName=" + userName + ", userKey=" + userKey + '}';
    }
    
    public JsonObject toJson()
    {
        return Json.createObjectBuilder()
                .add("apikey", apiKey)
                .add("username", userName)
                .add("userkey", userKey)
                .build();
    }
    
}
