/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb.model;

import com.iamcodepoet.thetvdb.util.EqualsBuilder;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import java.util.Optional;

/**
 *Data object for a series episode.
 * @author roberto
 */
public class Episode
{
    private int id;
    private String imdbId;
    private int season;
    private int seasonId;
    private int number;
    private int numberInSeries;
    private String name;
    private int seriesId;
    private Optional<LocalDate> firstAired;
    private String[] guestStars = new String[0];
    private String[] directors = new String[0];
    private String[] writers = new String[0];
    private String overview;
    private String productionCode;
    private Optional<Graphic> thumbnail = Optional.empty();
    
    private String dvdDiskId;
    private int dvdSeason;
    private int dvdEpisodeNumber;
    private int dvdChapter;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getImdbId()
    {
        return imdbId;
    }

    public void setImdbId(String imdbId)
    {
        this.imdbId = imdbId;
    }

    public int getSeason()
    {
        return season;
    }

    public void setSeason(int season)
    {
        this.season = season;
    }

    public int getSeasonId()
    {
        return seasonId;
    }

    public void setSeasonId(int seasonId)
    {
        this.seasonId = seasonId;
    }

    public int getNumber()
    {
        return number;
    }

    public void setNumber(int number)
    {
        this.number = number;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getSeriesId()
    {
        return seriesId;
    }

    public void setSeriesId(int seriesId)
    {
        this.seriesId = seriesId;
    }

    public int getNumberInSeries()
    {
        return numberInSeries;
    }

    public void setNumberInSeries(int numberInSeries)
    {
        this.numberInSeries = numberInSeries;
    }

    public String getDvdDiskId()
    {
        return dvdDiskId;
    }

    public void setDvdDiskId(String dvdDiskId)
    {
        this.dvdDiskId = dvdDiskId;
    }

    public int getDvdSeason()
    {
        return dvdSeason;
    }

    public void setDvdSeason(int dvdSeason)
    {
        this.dvdSeason = dvdSeason;
    }

    public int getDvdEpisodeNumber()
    {
        return dvdEpisodeNumber;
    }

    public void setDvdEpisodeNumber(int dvdEpisodeNumber)
    {
        this.dvdEpisodeNumber = dvdEpisodeNumber;
    }

    public int getDvdChapter()
    {
        return dvdChapter;
    }

    public void setDvdChapter(int dvdChapter)
    {
        this.dvdChapter = dvdChapter;
    }

    public Optional<LocalDate> getFirstAired()
    {
        return firstAired;
    }

    public void setFirstAired(Optional<LocalDate> firstAired)
    {
        this.firstAired = firstAired;
    }
    
    public void setFirstAired(String string)
    {
        try
        {
            LocalDate date = LocalDate.parse(string);
            firstAired = Optional.of(date);
        }
        catch(DateTimeParseException ignore)
        {
            firstAired = Optional.empty();
        }
    }

    public String[] getGuestStars()
    {
        return guestStars;
    }

    public void setGuestStars(String[] guestStars)
    {
        this.guestStars = guestStars;
    }

    public String[] getDirectors()
    {
        return directors;
    }

    public void setDirectors(String[] directors)
    {
        this.directors = directors;
    }

    public String[] getWriters()
    {
        return writers;
    }

    public void setWriters(String[] writers)
    {
        this.writers = writers;
    }

    public String getOverview()
    {
        return overview;
    }

    public void setOverview(String overview)
    {
        this.overview = overview;
    }

    public String getProductionCode()
    {
        return productionCode;
    }

    public void setProductionCode(String productionCode)
    {
        this.productionCode = productionCode;
    }

    public Optional<Graphic> getThumbnail()
    {
        return thumbnail;
    }

    public void setThumbnail(Optional<Graphic> thumbnail)
    {
        this.thumbnail = thumbnail;
    }
    
    public void setThumbnail(Graphic graphic)
    {
        this.thumbnail = Optional.ofNullable(graphic);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(season,number,name);
    }

    @Override
    public boolean equals(Object obj)
    {
        return EqualsBuilder.withTest(Episode::getSeason)
                .appendTest(Episode::getNumber)
                .appendTest(Episode::getName)
                .areEqual(this, obj);
                
    }

    
    @Override
    public String toString()
    {
        
        return String.format("S%02dE%02d - %s", season,number,name);
    }
    
    
}
