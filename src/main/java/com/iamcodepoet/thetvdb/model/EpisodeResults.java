/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author roberto
 */
public class EpisodeResults
{
    private int page;
    private int pageCount;
    private List<Episode> episodes = new ArrayList<>();

    public boolean hasMorePages()
    {
        return pageCount > page;
    }
    
    public int getNextPage()
    {
        if(hasMorePages())
        {
            return page+1;
        }
        
        throw new IllegalStateException("There are no more pages available");
    }
    
    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

    public int getPageCount()
    {
        return pageCount;
    }

    public void setPageCount(int pageCount)
    {
        this.pageCount = pageCount;
    }

    public List<Episode> getEpisodes()
    {
        return episodes;
    }

    public void setEpisodes(List<Episode> episodes)
    {
        this.episodes = episodes;
    }
    
    public void addEpisode(Episode episode)
    {
        episodes.add(episode);
    }
}
