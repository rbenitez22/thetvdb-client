/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb.model;

import com.iamcodepoet.thetvdb.util.EqualsBuilder;
import java.util.Objects;

/**
 * A data object for graphics.
 *
 * @author roberto
 */
public class Graphic
{

    private int id;
    private String keyType;
    private String subKey;
    private String fileName;
    private int languageId;
    private Resolution resolution;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getKeyType()
    {
        return keyType;
    }

    public void setKeyType(String keyType)
    {
        this.keyType = keyType;
    }

    public String getSubKey()
    {
        return subKey;
    }

    public void setSubKey(String subKey)
    {
        this.subKey = subKey;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public int getLanguageId()
    {
        return languageId;
    }

    public void setLanguageId(int languageId)
    {
        this.languageId = languageId;
    }

    public Resolution getResolution()
    {
        return resolution;
    }

    public void setResolution(Resolution resolution)
    {
        this.resolution = resolution;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj)
    {
        return EqualsBuilder.withTest(Graphic::getId)
                .areEqual(this, obj);
    }

    @Override
    public String toString()
    {
        return fileName;
    }
}
