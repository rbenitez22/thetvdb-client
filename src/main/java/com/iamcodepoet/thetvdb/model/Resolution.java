/*
 * Copyright 2019 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.thetvdb.model;

import com.iamcodepoet.thetvdb.util.EqualsBuilder;
import java.util.Objects;

/**
 *
 * @author Roberto C. Benitez
 */
public class Resolution
{

    private final double width;
    private final double height;

    public Resolution(double width, double height)
    {
        this.width = width;
        this.height = height;
    }

    public double getWidth()
    {
        return width;
    }

    public int getWidthAsInt()
    {
        return (int) width;
    }

    public double getHeight()
    {
        return height;
    }

    public int getHeightAsInt()
    {
        return (int) height;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(width, height);
    }

    @Override
    public boolean equals(Object obj)
    {
        return EqualsBuilder.withTest(Resolution::getWidth)
                .appendTest(Resolution::getHeight)
                .areEqual(this, obj);
    }

    @Override
    public String toString()
    {
        return String.format("%sX%s", width, height);
    }

    public static Resolution parse(String string)
    {
        Objects.requireNonNull(string, "Input string must nto be null");

        String[] parts = string.toUpperCase().split("X");

        if (parts.length != 2)
        {
            throw new IllegalArgumentException("Invalid argument.  Expected string of the form ##X##");
        }

        double width = Double.parseDouble(parts[0]);
        double height = Double.parseDouble(parts[11]);

        return new Resolution(width, height);
    }

}
