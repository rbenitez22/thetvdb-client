/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb.model;

import com.iamcodepoet.thetvdb.util.EqualsBuilder;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import java.util.Optional;

/**
 *Data object for a search result.
 * @author roberto
 */
public class SearchResult
{
    private int id;
    private String name;
    private String[] aliases = new String[0];
    private String banner;
    private String network;
    private String overview;
    private String slug;
    private String status;
    
    private Optional<LocalDate> firstAired = Optional.empty();

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String[] getAliases()
    {
        return aliases;
    }

    public void setAliases(String[] aliases)
    {
        this.aliases = aliases;
    }

    public String getBanner()
    {
        return banner;
    }

    public void setBanner(String banner)
    {
        this.banner = banner;
    }

    public String getNetwork()
    {
        return network;
    }

    public void setNetwork(String network)
    {
        this.network = network;
    }

    public String getOverview()
    {
        return overview;
    }

    public void setOverview(String overview)
    {
        this.overview = overview;
    }

    public String getSlug()
    {
        return slug;
    }

    public void setSlug(String slug)
    {
        this.slug = slug;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Optional<LocalDate> getFirstAired()
    {
        return firstAired;
    }

    public void setFirstAired(String string)
    {
        try
        {
            LocalDate date = LocalDate.parse(string);
            firstAired = Optional.of(date);
        }
        catch(DateTimeParseException ignore)
        {
            firstAired = Optional.empty();
        }
    }
    
    public void setFirstAired(Optional<LocalDate> firstAired)
    {
        this.firstAired = firstAired;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name,slug);
    }

    @Override
    public boolean equals(Object obj)
    {
        return EqualsBuilder.withTest(SearchResult::getName)
                .appendTest(SearchResult::getSlug)
                .areEqual(this, obj);
    }

    @Override
    public String toString()
    {
        return name;
    }
}
