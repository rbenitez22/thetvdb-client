/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb.model;

import com.iamcodepoet.thetvdb.util.EqualsBuilder;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import java.util.Optional;

/**
 *A data object for a TV series.
 * @author roberto
 */
public class Series
{
    private int id;
    private String name;
    private String[] aliases = new String[0];
    private String banner;
    private String seriesId;
    private String status;
    private Optional<LocalDate> firstAired = Optional.empty();
    private String network;
    private String runtime;
    private String[] genre = new String[0];
    private String overview;
    private String rating;
    private String imdbId;
    private String zap2ItId;
    private String slug;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String[] getAliases()
    {
        return aliases;
    }

    public void setAliases(String[] aliases)
    {
        this.aliases = aliases;
    }

    public String getBanner()
    {
        return banner;
    }

    public void setBanner(String banner)
    {
        this.banner = banner;
    }

    public String getSeriesId()
    {
        return seriesId;
    }

    public void setSeriesId(String seriesId)
    {
        this.seriesId = seriesId;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Optional<LocalDate> getFirstAired()
    {
        return firstAired;
    }

    public void setFirstAired(Optional<LocalDate> firstAired)
    {
        this.firstAired = firstAired;
    }
    
    public void setFirstAired(String string)
    {
        try
        {
            LocalDate date = LocalDate.parse(string);
            firstAired = Optional.of(date);
        }
        catch(DateTimeParseException ignore)
        {
            firstAired = Optional.empty();
        }
    }

    public String getNetwork()
    {
        return network;
    }

    public void setNetwork(String network)
    {
        this.network = network;
    }

    public String getRuntime()
    {
        return runtime;
    }

    public void setRuntime(String runtime)
    {
        this.runtime = runtime;
    }

    public String[] getGenre()
    {
        return genre;
    }

    public void setGenre(String[] genre)
    {
        this.genre = genre;
    }

    public String getOverview()
    {
        return overview;
    }

    public void setOverview(String overview)
    {
        this.overview = overview;
    }

    public String getRating()
    {
        return rating;
    }

    public void setRating(String rating)
    {
        this.rating = rating;
    }

    public String getImdbId()
    {
        return imdbId;
    }

    public void setImdbId(String imdbId)
    {
        this.imdbId = imdbId;
    }

    public String getZap2ItId()
    {
        return zap2ItId;
    }

    public void setZap2ItId(String zap2ItId)
    {
        this.zap2ItId = zap2ItId;
    }

    public String getSlug()
    {
        return slug;
    }

    public void setSlug(String slug)
    {
        this.slug = slug;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name,slug);
    }

    @Override
    public boolean equals(Object obj)
    {
       return EqualsBuilder.withTest(Series::getName)
               .appendTest(Series::getSlug)
               .areEqual(this, obj);
    }

    @Override
    public String toString()
    {
        return name;
    }
    
    
}
