/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 *A helper class to build an <code>equals</code> test.
 * @author Roberto C. Benitez
 * @param <T> type of object being tested
 */
public class EqualsBuilder<T>
{

    private final List<Function<T, Object>> functions = new LinkedList<>();

    public EqualsBuilder<T> appendTest(Function<T, Object> function)
    {
        functions.add(function);
        return this;
    }

    public boolean areEqual(T object, Object otherObject)
    {
        if (object == otherObject)
        {
            return true;
        }

        if (object == null || otherObject == null)
        {
            return false;
        }

        if (object.getClass() != otherObject.getClass())
        {
            return false;
        }

        final T other = (T) otherObject;

        for (Function<T, Object> function : functions)
        {
            Object value1 = function.apply(object);
            Object value2 = function.apply(other);
            if (!Objects.equals(value1, value2))
            {
                return false;
            }
        }

        return true;
    }

    public static <T> EqualsBuilder<T> withTest(Function<T, Object> function)
    {
        EqualsBuilder<T> builder = new EqualsBuilder<>();
        builder.appendTest(function);

        return builder;
    }
}