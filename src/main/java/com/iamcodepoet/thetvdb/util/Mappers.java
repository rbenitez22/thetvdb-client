/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb.util;

import com.iamcodepoet.thetvdb.model.Episode;
import com.iamcodepoet.thetvdb.model.Graphic;
import com.iamcodepoet.thetvdb.model.Resolution;
import com.iamcodepoet.thetvdb.model.SearchResult;
import com.iamcodepoet.thetvdb.model.Series;
import java.util.function.Function;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;

/**
 * A utility class with mapping functions.
 *
 * @author roberto
 */
public final class Mappers
{

    private Mappers()
    {
    }

    public static Function<JsonValue, Episode> jsonValueToEpisode()
    {
        return value -> jsonObjectToEpisode().apply(value.asJsonObject());
    }

    public static Function<JsonObject, Episode> jsonObjectToEpisode()
    {
        return json ->
        {
            int id = json.getInt("id", 0);
            int season = json.getInt("airedSeason", 0);
            int seasonId = json.getInt("airedSeasonID", 0);
            int number = json.getInt("airedEpisodeNumber", 0);
            int numberInSeries = json.getInt("absoluteNumber", 0);

            String name = json.getString("episodeName", "");
            String firstAired = json.getString("firstAired", "");
            String[] guestStarts = convertJsonArrayToJavaArray(json, "guestStars");
            String[] directors = convertJsonArrayToJavaArray(json, "directors");
            String[] writers = convertJsonArrayToJavaArray(json, "writers");

            String overview = json.getString("overview", "");
            String productionCode = json.getString("productionCode", "");
            String imdbId = json.getString("imdbId", "");
            int seriesId = json.getInt("seriesId", 0);

            String dvdDiskId = json.getString("dvdDiskid", "");
            int dvdSeason = json.getInt("dvdSeason", 0);
            int dvdEpisodeNumber = json.getInt("dvdEpisodeNumber", 0);
            int dvdChapter = json.getInt("dvdChapter", 0);

            Graphic graphic = getEpisodeThumbnail(json);

            Episode episode = new Episode();
            episode.setId(id);
            episode.setSeason(season);
            episode.setSeasonId(seasonId);
            episode.setImdbId(imdbId);
            episode.setNumber(number);
            episode.setName(name);
            episode.setFirstAired(firstAired);
            episode.setOverview(overview);
            episode.setGuestStars(guestStarts);
            episode.setDirectors(directors);
            episode.setWriters(writers);
            episode.setProductionCode(productionCode);
            episode.setSeriesId(seriesId);
            episode.setThumbnail(graphic);
            episode.setNumberInSeries(numberInSeries);
            episode.setDvdDiskId(dvdDiskId);
            episode.setDvdSeason(dvdSeason);
            episode.setDvdEpisodeNumber(dvdEpisodeNumber);
            episode.setDvdChapter(dvdChapter);

            return episode;
        };
    }

    private static Graphic getEpisodeThumbnail(JsonObject json)
    {
        String fileName = json.getString("filename", "");
        int width = getStringAsInt(json, "thumbWidth");
        int height = getStringAsInt(json, "thumbHeight");

        Graphic graphic;
        if (fileName == null || fileName.trim().isEmpty()
                || width == 0 || height == 0)
        {
            graphic = null;
        }
        else
        {
            graphic = new Graphic();
            graphic.setFileName(fileName);
            graphic.setKeyType("episodes");
            graphic.setResolution(new Resolution(width, height));
        }
        return graphic;
    }

    public static Function<JsonObject, Series> jsonObjectToSeries()
    {
        return json ->
        {
            int id = json.getInt("id", 0);
            String name = json.getString("seriesName", "");
            String[] aliases = convertJsonArrayToJavaArray(json, "aliases");
            String banner = json.getString("banner", "");
            String seriesId = json.getString("seriesId", "");
            String status = json.getString("status", "");
            String firstAired = json.getString("firstAired", "");
            String network = json.getString("network", "");
            String runtime = json.getString("runtime", "");
            String[] genre = convertJsonArrayToJavaArray(json, "genre");
            String overview = json.getString("overview", "");
            String rating = json.getString("rating");
            String imdbId = json.getString("imdbId", "");
            String zap2ItId = json.getString("zap2itId", "");
            String slug = json.getString("slug", "");

            Series series = new Series();
            series.setId(id);
            series.setName(name);
            series.setAliases(aliases);
            series.setBanner(banner);
            series.setSlug(status);
            series.setFirstAired(firstAired);
            series.setSeriesId(seriesId);
            series.setNetwork(network);
            series.setRuntime(runtime);
            series.setGenre(genre);
            series.setOverview(overview);
            series.setRating(rating);
            series.setImdbId(imdbId);
            series.setZap2ItId(zap2ItId);
            series.setSlug(slug);

            return series;
        };
    }

    public static Function<JsonValue, SearchResult> jsonToSearchResult()
    {
        return value ->
        {
            JsonObject json = value.asJsonObject();
            int id = json.getInt("id", 0);
            String name = json.getString("seriesName", "");
            String key = "aliases";
            String[] aliases = convertJsonArrayToJavaArray(json, key);
            String banner = json.getString("banner", "");
            String network = json.getString("network");
            String overview = json.getString("overview", "");
            String slug = json.getString("slug", "");
            String status = json.getString("status", "Unknown");
            String firstAired = json.getString("firstAired", "");

            SearchResult result = new SearchResult();
            result.setId(id);
            result.setName(name);
            result.setAliases(aliases);
            result.setBanner(banner);
            result.setNetwork(network);
            result.setOverview(overview);
            result.setSlug(slug);
            result.setStatus(status);
            result.setFirstAired(firstAired);

            return result;
        };
    }

    private static String[] convertJsonArrayToJavaArray(JsonObject json, String key)
    {
        return json.getJsonArray(key).stream()
                .map(e -> ((JsonString) e).getString()).toArray(String[]::new);
    }

    private static int getStringAsInt(JsonObject json, String key)
    {
        String string = json.getString(key, "0");
        if (string == null || string.trim().isEmpty())
        {
            return 0;
        }

        try
        {
            return Integer.parseInt(string);
        }
        catch (NumberFormatException ignore)
        {
            return 0;
        }
    }
}
