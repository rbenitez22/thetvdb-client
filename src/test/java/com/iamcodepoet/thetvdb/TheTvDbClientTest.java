/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb;

import com.iamcodepoet.thetvdb.model.Credentials;
import com.iamcodepoet.thetvdb.model.Episode;
import com.iamcodepoet.thetvdb.model.EpisodeResults;
import com.iamcodepoet.thetvdb.model.Graphic;
import com.iamcodepoet.thetvdb.model.SearchResult;
import com.iamcodepoet.thetvdb.model.Series;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import org.junit.Test;
import org.junit.Before;

/**
 *
 * @author roberto
 */
public class TheTvDbClientTest
{
    private Credentials credentails;
    
    @Before public void loadCredentials()
    {
        Path path = Paths.get(System.getProperty("user.home"), ".TheTvDb","api-cred.properties");
        try(InputStream is = Files.newInputStream(path))
        {
            Properties props = new Properties();
            props.load(is);
            
            credentails = new Credentials();
            credentails.setApiKey(props.getProperty("apiKey"));
            credentails.setUserKey(props.getProperty("userKey"));
            credentails.setUserName(props.getProperty("userName"));
        }
        catch(IOException e)
        {
            throw new RuntimeException("Unable to load credentails.");
        }
    }
    
    public TheTvDbClientTest()
    {
    }
    
    @Test public void testGetSeries()
    {
        TheTvDbClient client = TheTvDbClient.withCredentials(credentails);
        Series series = client.getSeries(80379);
        System.out.println(series);
    }
    
    @Test public void testGetEpisodes()
    {
        TheTvDbClient client = TheTvDbClient.withCredentials(credentails);
        
        EpisodeQuery query = EpisodeQuery.forSeriesId(80379).withPage(10);
                
        
        EpisodeResults results = client.getEpisodes(query);
        
        System.out.printf("Page  %s of %s%n",results.getPage(),results.getPageCount());
        for(Episode episode : results.getEpisodes())
        {
            System.out.println(episode);
        }
    }
    
    @Test public void testDownloadGraphic()
    {
        String name="episodes_80379_387721.jpg";
        
        TheTvDbClient client= new TheTvDbClient();
        client.login(credentails);
        
        Path local = Paths.get("/tmp", name);
        try
        {
            Graphic graphic = new Graphic();
            graphic.setFileName("episodes/80379/387721.jpg");
            client.downloadGraphic(graphic, local);
        } 
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    @Test public void testSearch()
    {
        TheTvDbClient client= new TheTvDbClient();
        client.login(credentails);
        
        SeriesQuery query = new SeriesQuery().withName("Outer Limits");
        List<SearchResult> results = client.search(query);
        
        for(SearchResult rst : results)
        {
            System.out.printf("[%s] %s (%s)%n",rst.getId(),rst.getName(),rst.getNetwork());
            Series series = client.getSeries(rst.getId());
            System.out.println("SERIES: " + series);
        }
        
    }

    @Test
    public void testLogin()
    {
        TheTvDbClient client= new TheTvDbClient();
        String token = client.login(credentails);
        System.out.println(token);
    }
    
    @Test public void testRefreshToken()
    {
        TheTvDbClient client= new TheTvDbClient();
        String token = client.login(credentails);
        String refreshedToken = client.refreshToken();
        
        System.out.println("ORIGINAL: " + token);
        System.out.println("REFRESHED: " + refreshedToken);
        System.out.println("IS SAME? "  + token.equals(refreshedToken));
    }
    
}
