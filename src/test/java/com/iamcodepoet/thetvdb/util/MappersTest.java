/*
 * Copyright 2019 roberto.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iamcodepoet.thetvdb.util;

import com.iamcodepoet.thetvdb.model.Episode;
import com.iamcodepoet.thetvdb.model.SearchResult;
import com.iamcodepoet.thetvdb.model.Series;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author roberto
 */
public class MappersTest
{
    
    public MappersTest()
    {
    }

    @Test public void testJsonToEpisode()
    {
        try(InputStream is =getClass().getResourceAsStream("single-episode.json");
                JsonReader reader = Json.createReader(is))
        {
            JsonObject root = reader.readObject();
            List<Episode> episodes = root.getJsonArray("data").stream()
                    .map(Mappers.jsonValueToEpisode())
                    .collect(Collectors.toList());
            
            Assert.assertEquals("Invalid Parsed Episode count", 1,episodes.size());
            
            Episode episode = episodes.get(0);
            Assert.assertEquals("Invalid Season", 2,episode.getSeason());
            Assert.assertEquals("Invalid Episode Nubmer", 1,episode.getNumber());
            Assert.assertEquals("Invalid Episode Name", "The Bad Fish Paradigm",episode.getName());
            
            String fileName = episode.getThumbnail().isPresent()? episode.getThumbnail().get().getFileName() : null;
            Assert.assertEquals("Invalid thumbnail file name","episodes/80379/387721.jpg",fileName);
            
            Assert.assertEquals("Invalid guest star count", 1,episode.getGuestStars().length);
            Assert.assertEquals("Invalid actor name for first guest star", "Carol Ann Susi",episode.getGuestStars()[0]);
            
            
        }
        catch(IOException e)
        {
            Assert.fail(e.getMessage());
        }
    }
    
    @Test public void testJsonToSeries()
    {
        try(InputStream is =getClass().getResourceAsStream("series.json");
                JsonReader reader = Json.createReader(is))
        {
            JsonObject root = reader.readObject();
            JsonObject data = root.getJsonObject("data");
            
            Series series = Mappers.jsonObjectToSeries().apply(data);
            Assert.assertEquals("Invalid Series Name","The Big Bang Theory", series.getName());
            
            LocalDate date = LocalDate.parse("2007-09-24");
            Assert.assertEquals("Invalid first aired date", date,series.getFirstAired().orElse(null));
            
            Assert.assertEquals("Invalid Genre list count",1, series.getGenre().length);
            Assert.assertEquals("Invalid First Genre","Comedy", series.getGenre()[0]);
            
            Assert.assertEquals("Invalid Alias count",1, series.getAliases().length);
            Assert.assertEquals("Invalid alias","Big Bang", series.getAliases()[0]);
            
        }
        catch(IOException e)
        {
            Assert.fail(e.getMessage());
        }
    }
    @Test
    public void testJsonToSearchResult()
    {
        try(InputStream is =getClass().getResourceAsStream("search-results.json");
                JsonReader reader = Json.createReader(is))
        {
            JsonObject root = reader.readObject();
            List<SearchResult> results = root.getJsonArray("data")
                    .stream().map(Mappers.jsonToSearchResult())
                    .collect(Collectors.toList());
            
            Assert.assertEquals("Invalid result count", 10,results.size());
            
            SearchResult first = results.get(0);
            Assert.assertEquals("Invalid name for first result","Big Bang",first.getName());
           Assert.assertEquals("Invalid alias count for first result", 0,first.getAliases().length);
            
            SearchResult second = results.get(1);
            Optional<LocalDate> date = second.getFirstAired();
            int secondYear = date.isPresent() ? date.get().getYear() : 0;
            Assert.assertEquals("Invalid release year for second result", 2007,secondYear);
            
            Assert.assertEquals("Invalid alias count for second result", 1,second.getAliases().length);
            Assert.assertEquals("Invalid alias for second result", "Big Bang",second.getAliases()[0]);
            
            
            
        }
        catch(IOException e)
        {
            Assert.fail(e.getMessage());
        }
    }
  
}
